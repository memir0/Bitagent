<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="no"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Kontrollpanelet</title>
        <meta name="description" content="Bitagent kontrollpanel">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="../img/icon.png">

        <!--fontawesome -->
        <link rel="stylesheet" href="../fonts/font-awesome/css/font-awesome.css">

        <!--Jquery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>

        <!--Google piechart-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        
<!--test-->

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!--Cryptocoins icons--> 
        <link rel="stylesheet" href="assets/icons/cryptocoins.css">

        <script>   
        google.charts.load("current", {packages:["corechart"]});
        var verdi = {"ETH": 0,"BTC": 0,"BCH": 0,"LTC": 0,"XRP": 0,"ZEC": 0,"MIOTA": 0,"ADA": 0}    
        var antall = {"ETH": 2.8,"BTC": 0.1,"BCH": 0.363,"LTC": 0.4,"XRP": 100,"ZEC": 0,"MIOTA": 0,"ADA": .001}  
        var kurs = {"ETH": 0,"BTC": 0,"BCH": 0,"LTC": 0,"XRP": 0,"ZEC": 0,"MIOTA": 0,"ADA": 0} 
        var totalverdi = 0  

            $(function(){
                var crypto = ["ETH","BTC","BCH","LTC","XRP","ZEC","MIOTA","ADA"]
                $.ajax({
                    type: "GET",
                    url: 'https://api.coinmarketcap.com/v1/ticker/?limit=30',
                    success: function(data){
                        for(var j = 0; j < 8; j++){
                            for(var i = 0; i < 30; i++){
                                if(crypto[j] == data[i].symbol){
                                    kurs[crypto[j]] = (data[i].price_usd)
                                    verdi[crypto[j]]=antall[crypto[j]]*kurs[crypto[j]]
                                    totalverdi += verdi[crypto[j]]
                                    i+=30
                                }
                            }
                        }
                    },
                    complete: function(data) {
                        document.getElementById("totalverdi").innerHTML = "Totalverdi: $" + totalverdi.toFixed(2);
                        drawChart();
                        }
                    }
                )
            }
            )
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
            ['Coin', 'USD'],
            ['Ethereum - ETH: $' + verdi["ETH"].toFixed(2),  verdi["ETH"]],
            ['Bitcoin - BTC: $' + verdi["BTC"].toFixed(2),  verdi["BTC"]],
            ['Bitcoin Cash - BCH: $' + verdi["BCH"].toFixed(2), verdi["BCH"]],
            ['Litecoin - LTC: $' + verdi["LTC"].toFixed(2), verdi["LTC"]],
            ['Ripple - XRP: $' + verdi["XRP"].toFixed(2), verdi["XRP"]],
            ['Zcash - ZEC: $' + verdi["ZEC"].toFixed(2), verdi["ZEC"]],
            ['IOTA - MIOTA: $' + verdi["MIOTA"].toFixed(2), verdi["MIOTA"]],
            ['Cardano - ADA: $' + verdi["ADA"].toFixed(2), verdi["ADA"]]
            ]);

            var options = {
            title: 'Total verdi: $' + totalverdi.toFixed(2),
            titleTextStyle: {color: "white"},
            backgroundColor: "transparent",
            fontSize: 20,
            colors: ["#282828", "#F7931A", "#8DC451", "#838383", "#346AA9", "#e5a93d", "#FFFFFF", "#3CC8C8"],
            legend: {position: 'right', textStyle: {color: 'white', fontSize: 16}},
            pieHole: 0.4,
            pieSliceTextStyle: {color: "black"},
            tooltipText: "percentage",
            sliceVisibilityThreshold: 0.0000001,
            chartArea:{left:0,top:0,width:'70%',height:'100%'}
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
            chart.draw(data, options);
        }
        </script>
    </head>
    <?php
        if(isset($_GET["feil"])){
            $message = $_GET["feil"];
            echo "<script type='text/javascript'>alert('$message');</script>";
        }
    ?>
    <body data-spy="scroll" data-target=".navbar-collapse">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header>            
                     <nav class="navbar navbar-default" id="navmenu">
                                <div class="container-fluid">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="http://localhost/bitagent/">
                                            <img src="assets/images/logo.png"/>
                                        </a>
                                        <div style="float: right;">
                                            <ul class="nav navbar-nav navbar-right">
                                                <li><a>USDT: $0</a></li>
                                                <li><a href="#home">Oversikt</a></li>
                                                <li><a href="#service">Min Konto</a></li>
                                            </ul>    
                                        </div>
                                    </div>

                                </div>
                            </nav>
        </header> <!--End of header -->

        <!--Donutchart-->
        <section id="home">
            <br>
            <center>
                <div id="donutchart" style="width: 900px; height: 500px;"></div>
            </center>
        </section>
        <h1 id="totalverdi">Totalverdi: <i class='fa fa-refresh'></i> Laster</h1>
        <center>
        <h2>Handlinger</h2>
        <div class="trade">
            <button onclick="overlay(1)" id="kjop">Kjøp</button>
            <button disabled onclick="overlay(2)" id="bytt">Bytt</button>
            <button onclick="overlay(3)" id="selg">Selg</button>
        </div>
        </center>
        <div id="overlay">
            <p id="x" onclick="overlay(0)">✖</p>
                <div id="transact" style="display: none;">
                    <form action='transaction.php' method="POST">
                        <ul class="tab-group">
                            <li class="tab"><a id="eth" onclick="select(0)">Ethereum<br><i class="cc ETH-alt"></i></a></li>
                            <li class="tab"><a id="btc" onclick="select(1)">Bitcoin<br><i class="cc BTC"></i></a></li>
                            <li class="tab"><a id="bch" onclick="select(2)">Bitcoin Cash<br><i class="cc BCH"></i></a></li>
                            <li class="tab"><a id="ltc" onclick="select(3)">Litecoin<br><i class="cc LTC"></i></a></li>
                            <li class="tab"><a id="xrp" onclick="select(4)">Ripple<br><i class="cc XRP-alt"></i></a></li>
                            <li class="tab"><a id="zec" onclick="select(5)">Zcash<br><i class="cc ZEC"></i></a></li>
                            <li class="tab"><a id="iota" onclick="select(6)">IOTA<br><i class="cc IOTA"></i></a></li>
                            <li class="tab"><a id="ada" onclick="select(7)">Cardano<br><i class="cc ADA"></i></a></li>
                            <li class="tab"><a id="annet" onclick="select(8)">Annet<br><br><input name="annet" type="text" size="10" style="font-size: 24px;"></a></li>
                        </ul>
                        <div style="display: none;" id="for">
                            
                                <input name="currency" id="currency" type="hidden">
                                <input name="type" id="type" type="hidden">
                                <p>For: $<input name="amount" type="number" value="100"></p><br>
                                <p>Pris per: $<input name="price" type="number" placeholder="Auto"></p>
                                <button id="submit" type="submit">Kjøp</button>
                            
                        </div>
                    </form>
                </div>
            <button onclick="overlay(0)" id="back">Tilbake</button>
        </div>
        <footer id="footer" class="footer">
           
        </footer>
        <script>
        //Global vars
        var i = 0;
        var selected = "";
        var cclist = ["eth", "btc", "bch", "ltc", "xrp", "zec", "iota", "ada", "annet"];
        var buttons = ["buy", "trade", "sell"]
        var buttonsTrans = ["Kjøp", "Bytt", "Selg"]

        function overlay(x) {
            if(x==0){
                document.getElementById("overlay").style.display = "none";
                document.getElementById("buy").style.display = "none";
                document.getElementById("trade").style.display = "none";
                document.getElementById("sell").style.display = "none";
            }
            else{
                document.getElementById("overlay").style.display = "block";
                document.getElementById("transact").style.display = "block";
                document.getElementById("type").value = buttons[x-1];
                document.getElementById("submit").innerHTML = buttonsTrans[x-1];
                document.getElementById("submit").className = buttons[x-1];
            }
        }
        function select(x){
                document.getElementById(cclist[x]).style = "background-color: rgb(40, 15, 120); color: purple;";
                document.getElementById("currency").value = cclist[x];

                if(selected != cclist[x]){
                    if(selected != ""){
                    document.getElementById(selected).style = "background-color: transparent; color: white;";
                    }
                    document.getElementById("for").style.display = "block";
                    selected = cclist[x];
                }
        }
        </script>
    </body>
</html>