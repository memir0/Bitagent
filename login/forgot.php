<?php 
/* Reset your password form, sends reset.php password link */
require 'db.php';
session_start();

// Check if form submitted with method="post"
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) 
{   
    $email = $mysqli->escape_string($_POST['email']);
    $result = $mysqli->query("SELECT * FROM users WHERE email='$email'");

    if ( $result->num_rows == 0 ) // User doesn't exist
    { 
        $_SESSION['message'] = "En bruker med den emailen eksiterer allerede!";
        header("location: error.php");
    }
    else { // User exists (num_rows != 0)

        $user = $result->fetch_assoc(); // $user becomes array with user data
        
        $email = $user['email'];
        $hash = $user['hash'];
        $first_name = $user['first_name'];

        // Session message to display on success.php
        $_SESSION['message'] = "<p>Vennligst sjekk din email <span>$email</span>"
        . " for en link for å gjennopprette passordet ditt!</p>";

        // Send registration confirmation link (reset.php)
        $to      = $email;
        $subject = 'Passord Gjenoppretting Link ( bitagent.no )';
        $message_body = '
        Hei '.$first_name.',

        Du har sendt beskjed om at du vil gjennopprette passordet ditt!

        Vennligst trykk her for å nullstille ditt passord:

        http://bitagent.no/login/reset.php?email='.$email.'&hash='.$hash;  

        mail($to, $subject, $message_body);

        header("location: success.php");
  }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Nullstill ditt passord</title>
  <?php include 'css/css.html'; ?>
  <link rel="icon" type="image/png" href="../img/icon.png">
</head>

<body>
    
  <div class="form">

    <h1>Nullstill ditt passord</h1>

    <form action="forgot.php" method="post">
     <div class="field-wrap">
      <label>
        Email<span class="req">*</span>
      </label>
      <input type="email"required autocomplete="off" name="email"/>
    </div>
    <button class="button button-block"/>Nullstill</button>
    </form>
  </div>
          
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="js/index.js"></script>
</body>

</html>
