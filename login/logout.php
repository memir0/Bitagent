<?php
/* Log out process, unsets and destroys session variables */
session_start();
session_unset();
session_destroy(); 
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Logget ut</title>
  <link rel="icon" type="image/png" href="../img/icon.png">
  <?php include 'css/css.html'; ?>
</head>

<body>
    <div class="form">
          <h1>Logget ut</h1>
              
          <p><?= 'Du er nå logget ut og kan lukke fannen'; ?></p>
          
          <a href="index.php"><button class="button button-block"/>Hjem</button></a>

    </div>
</body>
</html>
